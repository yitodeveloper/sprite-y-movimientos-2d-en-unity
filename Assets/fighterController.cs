﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fighterController : MonoBehaviour {

    private float velocidad = 8f;
    private float padding = 1f;
    public GameObject bullet;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float vAxis = Input.GetAxis("Vertical");
        transform.position += new Vector3(0, vAxis * velocidad* Time.deltaTime, 0);
        float hAxis = Input.GetAxis("Horizontal");
        transform.position += new Vector3(hAxis * velocidad * Time.deltaTime,0, 0);

        float newX = Mathf.Clamp(transform.position.x, -10f+padding, 10f-padding);
        float newY = Mathf.Clamp(transform.position.y, -10f+padding, 10f-padding);
        transform.position = new Vector3(newX,newY,0);

        if (Input.GetKeyDown(KeyCode.Space)) {
            InvokeRepeating("fire",0.001f,0.1f);
        }else if (Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke("fire");
        }
    }

    void fire() {
        var fighter = GameObject.Find("Fighter");
        if (fighter != null)
        {
            Vector3 newLeftPosition = fighter.transform.position + Vector3.up * 1.5f + Vector3.left * 0.65f;
            Vector3 newRightPosition = fighter.transform.position + Vector3.up * 1.5f + Vector3.right * 0.65f;
            Instantiate(bullet, newLeftPosition, Quaternion.identity);
            Instantiate(bullet, newRightPosition, Quaternion.identity);
        }
    }
}
