﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fighterBulletController : MonoBehaviour {

    public float bulletSpeed = 16f;
	// Use this for initialization
	void Start () {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0f,bulletSpeed,0f);
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y >= 11f) {
            Destroy(gameObject);
        }
	}
}
